<?php

namespace MEDIAESSENZ\Spreadshop\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Alexander Grein <alexander.grein@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Last Modified:
 * Date: 2019-04-30
 * Time: 18:23
 */
class PageLayoutView
{
    const LLPATH = 'LLL:EXT:spreadshop/Resources/Private/Language/locallang.xlf:';

    const SHOW_SETTINGS = ['script', 'tld', 'name', 'locale', 'updateMetadata', 'usePushState', 'startToken', 'swipeMenu'];

    /**
     * Returns information about this extension's pi1 plugin
     *
     * @param array $params Parameters to the hook
     * @return string Information about pi1 plugin
     */
    public function getExtensionSummary(array $params)
    {
        $flexFormData = GeneralUtility::xml2array($params['row']['pi_flexform'])['data'];

        $result = '<div class="row">';
        $result .= '<div class="col-sm-2"><img src="../../typo3conf/ext/spreadshop/Resources/Public/Icons/Extension.svg" /></div>';
        $result .= '<div class="col-sm-10">';
        $result .= '<h3 style="margin-top: .2em">Spreadshop</h3>';
        $result .= '<pre style="white-space:normal; background:transparent; padding: 0;border:0 none;">';

        foreach (self::SHOW_SETTINGS as $setting) {
            $result .= '<strong>' . self::getLanguageService()->sL(self::LLPATH . $setting) . ':</strong>' . ' ' . self::getFieldFromFlexform($flexFormData, 'settings.' . $setting) . '<br />';
        }

        $result .= '</pre>';
        $result .= '</div>';
        $result .= '</div>';

        return $result;
    }

    /**
     * Get field value from flexform configuration,
     * including checks if flexform configuration is available
     *
     * @param array $flexform flexform data
     * @param string $key name of the key
     * @param string $sheet name of the sheet
     * @return string|null if nothing found, value if found
     */
    protected static function getFieldFromFlexform($flexform, $key, $sheet = 'main')
    {
        if ($flexform) {
            if (is_array($flexform) && is_array($flexform[$sheet]) && is_array($flexform[$sheet]['lDEF'])
                && is_array($flexform[$sheet]['lDEF'][$key]) && isset($flexform[$sheet]['lDEF'][$key]['vDEF'])
            ) {
                return $flexform[$sheet]['lDEF'][$key]['vDEF'];
            }
        }

        return null;
    }

    /**
     * Return language service instance
     *
     * @return \TYPO3\CMS\Core\Localization\LanguageService
     */
    protected static function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }
}
