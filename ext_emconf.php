<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "spreadshop".
 *
 * Auto generated 30-04-2019 12:27
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['spreadshop'] = [
    'title' => 'Spreadshop',
    'description' => 'Integrate a spreadshop',
    'category' => 'plugin',
    'version' => '1.0.2',
    'state' => 'stable',
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-12.4.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'MEDIAESSENZ\\Spreadshop\\' => 'Classes',
        ],
    ],
];
