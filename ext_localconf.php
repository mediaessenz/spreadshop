<?php

defined('TYPO3') or die();

(function () {
    // register plugin
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Spreadshop',
        'Shop',
        [
            \MEDIAESSENZ\Spreadshop\Controller\ShopController::class => 'show',
        ]
    );

    // Add PageTS
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:spreadshop/Configuration/TsConfig/Page.tsconfig">');

    // Page module hook
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['list_type_Info']['spreadshop_shop']['spreadshop'] =
        'MEDIAESSENZ\\Spreadshop\\Hooks\\PageLayoutView->getExtensionSummary';
})();
