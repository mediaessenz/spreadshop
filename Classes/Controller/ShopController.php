<?php

namespace MEDIAESSENZ\Spreadshop\Controller;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class ShopController extends ActionController
{
    public function showAction(): ResponseInterface
    {
        // @extensionScannerIgnoreLine
        $this->view->assign('data', $this->configurationManager->getContentObject()->data);
        return $this->htmlResponse();
    }
}
