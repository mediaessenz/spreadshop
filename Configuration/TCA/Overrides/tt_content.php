<?php

defined('TYPO3') or die();

call_user_func(function () {

    // register confirmation plugin
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'Spreadshop',
        'Shop',
        'Spreadshop',
        'EXT:spreadshop/Resources/Public/Icons/Extension.svg'
    );

    // add FlexForm for article plugin
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['spreadshop_shop'] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        'spreadshop_shop',
        'FILE:EXT:spreadshop/Configuration/FlexForms/Shop.xml'
    );

    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['spreadshop_shop'] = 'layout,select_key,pages,recursive';
});
